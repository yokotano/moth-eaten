FROM alpine:3.9.4

LABEL maintainer="Yokota Ryoichi <r.yokota14@gmail.com>"

RUN apk add --upgrade zip

ENTRYPOINT [ "zip" ]
CMD [ "--help" ]
